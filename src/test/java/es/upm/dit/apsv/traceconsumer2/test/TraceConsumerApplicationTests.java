package es.upm.dit.apsv.traceconsumer2.test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.fail;


import java.util.NoSuchElementException;

import java.util.function.Consumer;

import java.util.function.Function;

import java.util.function.Supplier;


import org.junit.jupiter.api.AfterEach;

import org.junit.jupiter.api.BeforeEach;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.cloud.stream.binder.test.InputDestination;

import org.springframework.cloud.stream.binder.test.TestChannelBinderConfiguration;

import org.springframework.context.annotation.Import;

import org.springframework.messaging.Message;

import org.springframework.messaging.support.GenericMessage;


import es.upm.dit.apsv.traceconsumer2.Traceconsumer2Application;

import es.upm.dit.apsv.traceconsumer2.model.Trace;

import es.upm.dit.apsv.traceconsumer2.model.TransportationOrder;

import es.upm.dit.apsv.traceconsumer2.repository.TransportationOrderRepository;
import java.util.Optional;


@SpringBootTest(classes = Traceconsumer2Application.class)

@Import(TestChannelBinderConfiguration.class) //se utiliza para mockear kafka

class TraceConsumerApplicationTests {


    @Autowired
    private InputDestination input;  //actúa como un productor de kafka


    @Autowired
    private TransportationOrderRepository repository; //en este caso se usa el repositorio real

    @BeforeEach  //anotación de JUnit, se ejecuta antes de los métodos de prueba
    void setUpOrders() {

        TransportationOrder o = new TransportationOrder(); //esta orden estará en la bbdd

        o.setToid("test-order"); //crea el objeto que luego se verá si se actualiza

        o.setTruck("test-truck");

        o.setOriginDate(100000000);

        o.setDstDate(o.getOriginDate() + (1000 * 60 * 12));

        o.setOriginLat(0.0);

        o.setOriginLong(0);

        o.setDstLat(44);

        o.setDstLong(88);

        o.setSt(0);

        repository.save(o);

    }


    @AfterEach //se ejecuta después de cada método de prueba

    void cleanUpOrders() {

        repository.deleteAll();

    }


    @Test
    void testOrderUpdate() {


        // 1. send the message to be processed asynchronously

        Trace t = new Trace("truck-1569233700000", "test-truck",

                  1569233700000L, 38.42089633723801,    -1.4491918734674392);

        Message<Trace> m = new GenericMessage<Trace>(t);

        input.send(m);  //simula escribir en la cola, lo recibe checkTrace()

        try {

       // asynchronous processing

            Thread.sleep(1000); //como es asíncrono, se usa para dar tiempo a actualizar la bbdd


        // 2. Check that the asynchronous function correctly updated the order

            TransportationOrder result = repository.findById("test-truck").

                                                               orElseThrow();

            assertEquals(result.getSt(), 0);  //comprobar que se actualiza el objeto en la bbdd

            assertEquals(result.getLastDate(), 1569233700000L);

            assertEquals(result.getLastLat(), 38.42089633723801);

            assertEquals(result.getLastLong(), -1.4491918734674392);


        } catch (NoSuchElementException e) {

            fail(); // the order should exist

        } catch (InterruptedException e) {

            e.printStackTrace();

        }


    }


   @Test
    void testOrderArrives() {  //comprueba que existe la orden y está activa, y además cambia el estado a 1
        // 1. send the message to be processed asynchronously
        Trace t = new Trace("truck-1569233700000", "test-truck",

                  1569233700000L, 45,    89);

        Message<Trace> m = new GenericMessage<Trace>(t);

        input.send(m);  //simula escribir en la cola, lo recibe checkTrace()

        // 2. Check that the asynchronous function correctly updated the order
        try {

            // asynchronous processing
     
                 Thread.sleep(1000); //como es asíncrono, se usa para dar tiempo a actualizar la bbdd
     
     
             // 2. Check that the asynchronous function correctly updated the order

             //no sé si era esto lo que había que hacer
     
                 TransportationOrder result = repository.findById("test-truck").
     
                                                                    orElseThrow();
     
                 assertEquals(result.getSt(), 1);  //comprobar que el estado pasa a 1 porque la distancia es menor que 10
     
                 assertEquals(result.getLastDate(), 1569233700000L);
     
                 assertEquals(result.getLastLat(), 45);
     
                 assertEquals(result.getLastLong(), 89);
     
     
             } catch (NoSuchElementException e) {
     
                 fail(); // the order should exist
     
             } catch (InterruptedException e) {
     
                 e.printStackTrace();
     
             }
     

    }


   @Test
    void testbadTrace() {
        Trace t = new Trace("truck-1234567891011", "test-truck2", 1569233700000L, 45,    89);

        Message<Trace> m = new GenericMessage<Trace>(t);

        input.send(m);

        try {
            Thread.sleep(1000); //como es asíncrono, se usa para dar tiempo a actualizar la bbdd
     
            Optional<TransportationOrder> result = repository.findById("test-truck2");
            assertFalse(result.isPresent());

        }
        catch (NoSuchElementException e) {        
            fail(); // the order should exist
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


}


