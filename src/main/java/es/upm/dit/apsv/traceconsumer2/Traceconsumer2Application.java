package es.upm.dit.apsv.traceconsumer2;

import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


import es.upm.dit.apsv.traceconsumer2.model.Trace;
import es.upm.dit.apsv.traceconsumer2.model.TransportationOrder;
import es.upm.dit.apsv.traceconsumer2.repository.TraceRepository;
import es.upm.dit.apsv.traceconsumer2.repository.TransportationOrderRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Optional;

@SpringBootApplication
public class Traceconsumer2Application {

	public static final Logger log = LoggerFactory.getLogger(Traceconsumer2Application.class);

	@Autowired
    private  TraceRepository tr;
	
	@Autowired
	private TransportationOrderRepository to;

	public static void main(String[] args) {
		SpringApplication.run(Traceconsumer2Application.class, args);
	}

	@Bean("consumer")
	public Consumer<Trace> checkTrace() {
			return t -> {
					t.setTraceId(t.getTruck() + t.getLastSeen());
					tr.save(t);

					TransportationOrder result = null;
					try {
						Optional<TransportationOrder> ot = to.findById(t.getTruck());
						if (ot.isPresent()) {
							result = ot.get();
							result.setLastDate(t.getLastSeen());
							result.setLastLat(t.getLat());
							result.setLastLong(t.getLng());
							if (result.distanceToDestination() < 10)
								result.setSt(1);
							
							TransportationOrder updated = to.save(result);
							if(updated != null)
								log.info("Order updated: " + result);
						}
					} 
					catch (Exception ex) {
						result = null;
					}			
			};
	}

}
